#ifndef _CRESULT_H_
#define _CRESULT_H_

#include <stdbool.h>

#define cr_t(type) cr_##type##_t
#define crptr_t(type) cr_t(type)*

#define declare_cr_t(type)  \
typedef struct              \
{                           \
    bool valid;              \
    type value;             \
} cr_t(type);

#endif