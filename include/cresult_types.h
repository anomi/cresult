#ifndef _CRESULT_TYPES_H_
#define _CRESULT_TYPES_H_

#include <stdint.h>
#include "cresult.h"

declare_cr_t(char);
declare_cr_t(short);
declare_cr_t(int);
declare_cr_t(float);
declare_cr_t(double);
declare_cr_t(int8_t);
declare_cr_t(int16_t);
declare_cr_t(int32_t);
declare_cr_t(int64_t);
declare_cr_t(uint8_t);
declare_cr_t(uint16_t);
declare_cr_t(uint32_t);
declare_cr_t(uint64_t);

#endif