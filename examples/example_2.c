#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cresult.h"

typedef struct
{
    char* left;
    char* right;
} char_set_t;

declare_cr_t(char_set_t);

cr_t(char_set_t) split_string(const char* s, int p) {
    cr_t(char_set_t) set = {
        .valid = false,
        .value = {
            .left = NULL,
            .right = NULL
        }
    };

    if (s != NULL
        && p <= strlen(s)) {
        char* left = (char*)calloc(sizeof(char) * p + 1, 0);
        char* right = (char*)calloc(sizeof(char) * (strlen(s) - p) + 1, 0);
        strncpy(left, s, p);
        strncpy(right, s + p, strlen(s) - p);

        set.value.left = left;
        set.value.right = right;
        set.valid = true;
    }

    return set;
}


int main(int argc, char const *argv[])
{
    cr_t(char_set_t) set = split_string("Hello World", 6);
    
    if (set.valid == true) {
        printf("left: %s, right: %s\n", set.value.left, set.value.right);
        
        free(set.value.left);
        free(set.value.right);
    } else {
        printf("Error!\n");
    }

    return 0;
}
