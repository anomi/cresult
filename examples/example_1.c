#include <stdio.h>
#include "cresult_types.h"

cr_t(float) div(float dividend, float divisor) {
    float quotient = 0;
    cr_t(float) r = { .valid = false, .value = 0 };
    if (divisor != 0) {
        r.valid = true;
        quotient = dividend / divisor;
        r.value = quotient;
    }
    return r;
}

int main(int argc, char const *argv[])
{
    cr_t(float) quotient = div(1 , 2);
    
    if (quotient.valid == true) {
        printf("quotient = %f\n", quotient.value);
    } else {
        printf("Error!\n");
    }

    return 0;
}
