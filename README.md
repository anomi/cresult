#  C Result Library
A utility library for a diffrent approache on retun values. A normal way could be something like this: 
```
    int div(float dividend, float divisor, float* quotient) {
        int r = -1;
        if (divisor != 0) {
            r = 0;
            *quotient = dividend / divisor;
        }
        return r;
    }
```
With the library you could achive something like this:
```
    declare_cr_t(float);

    cr_t(float) div(float dividend, float divisor) {
        float quotient = 0;
        cr_t(float) r = { .valid = false, .value = 0 };
        if (divisor != 0) {
            r.valid = true;
            quotient = dividend / divisor;
            r.value = quotient;
        }
        return r;
    }
```
With this approach the interface of the function stays clean and the return value does not need to be passed as a pointer to the function. 

# Known Limitations

```
    /* Variant A */
    // wont work
    declare_cr_t(unsigned int);
    // use instead (or typedef in stdint.h)
    typedef unsigned int uint32_t;
    declare_cr_t(uint32_t);

    /* Variant B */
    // wont work
    declare_cr_t(int*);
    // use instead (or typedef in stdint.h)
    typdef int* intptr_t;
    declare_cr_t(intptr_t);
```
I personally would not use the library in combination with returning pointers. Return a NULL pointer to indicate an error is a more appropriate variant. 
